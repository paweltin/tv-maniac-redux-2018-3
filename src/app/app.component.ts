import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { State } from './reducers';
import { map } from 'rxjs/operators';
import { Second, Year } from './actions/clock.actions';
import { interval } from 'rxjs/observable/interval';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  clock$: Observable<Date>;
  click$ = new Subject();

  constructor(store: Store<State>) {
    this.clock$ = store.select('clock').pipe(
      map(({date}) => date),
    );

    this.click$.subscribe(() => store.dispatch(new Year(1)));

    interval(5000).subscribe(() => store.dispatch(new Second(2)));
  }
}
