import { Action } from '@ngrx/store';

export enum ClockActionTypes {
  Year = '[Clock] Year',
  Second = '[Clock] Second'
}

export class Year implements Action {
  readonly type = ClockActionTypes.Year;

  constructor(public payload = 1) {
  }
}

export class Second implements Action {
  readonly type = ClockActionTypes.Second;

  constructor(public payload = 1) {
  }
}

export type ClockActions
  = Year
  | Second;

