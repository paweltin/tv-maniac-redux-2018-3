import { Action } from '@ngrx/store';
import { ClockActions, ClockActionTypes } from '../actions/clock.actions';


export interface State {
  date: Date;
}

export const initialState: State = {
  date: new Date()
};

export function reducer(state = initialState, action: ClockActions): State {
  const date = new Date(state.date);

  switch (action.type) {
    case ClockActionTypes.Year:
      date.setFullYear(date.getFullYear() + action.payload);
      return {date};

    case ClockActionTypes.Second:
      date.setSeconds(date.getSeconds() + action.payload);
      return {date};

    default:
      return state;
  }
}
