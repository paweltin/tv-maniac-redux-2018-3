import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { reducer as clock, State as Clock } from './clock.reducer';

export interface State {
  clock: Clock;
}

export const reducers: ActionReducerMap<State> = {
  clock
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
